package co.edu.udea.edatos.dao.impl;

import co.edu.udea.edatos.dao.EmpleadoDAO;
import co.edu.udea.edatos.modelo.Empleado;

import java.util.ArrayList;
import java.util.List;

public class EmpleadoDAOList implements EmpleadoDAO {

    public static List<Empleado> empleadosBD = new ArrayList<>();

    @Override
    public void guardarEmpleado(Empleado empleado) {
        //todo verificar si el empleado existía previamente o no
        System.out.println("en el dao");
        empleadosBD.add(empleado);
    }

    @Override
    public Empleado consultarEmpleadoPorIdentificacion(String id) {
        //todo retornar un Optional
        for(Empleado e: empleadosBD){
            if(id.equals(e.getIdentificacion())){
                return e;
            }
        }
        return null;
    }

    @Override
    public List<Empleado> consultarEmpleados() {
        //todo enviar una copia de la lista
        return empleadosBD;
    }
}
