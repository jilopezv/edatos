package co.edu.udea.edatos.bsn;

import co.edu.udea.edatos.dao.EmpleadoDAO;
import co.edu.udea.edatos.dao.impl.EmpleadoDAOList;
import co.edu.udea.edatos.modelo.Empleado;

public class EmpleadoBsn {

    private EmpleadoDAO empleadoDAO;

    public EmpleadoBsn(){
        empleadoDAO = new EmpleadoDAOList();
    }

    public void guardarEmpleado(Empleado empleado){
        //todo validar reglas de negocio
        //todo aplicar lógica de casos de uso
        System.out.println("En el bsn");
        this.empleadoDAO.guardarEmpleado(empleado);
    }
}
