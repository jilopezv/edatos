import co.edu.udea.edatos.bsn.EmpleadoBsn;
import co.edu.udea.edatos.modelo.Empleado;

import java.util.Scanner;

public class Principal {



    public static void main(String[] args) {
        EmpleadoBsn empleadoBsn = new EmpleadoBsn();
        Scanner lector = new Scanner(System.in);
        System.out.println("Ingrese el id: ");
        String identificacion = lector.next();
        System.out.println("Ingrese los nombres: ");
        String nombres = lector.next();
        System.out.println("Ingrese los apellidos: ");
        String apellidos = lector.next();
        System.out.println("Ingrese el cargo: ");
        String cargo = lector.next();

        Empleado empleado= new Empleado(identificacion, nombres, apellidos, cargo);
        empleadoBsn.guardarEmpleado(empleado);

    }
}
